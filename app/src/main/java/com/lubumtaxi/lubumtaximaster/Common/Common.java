package com.lubumtaxi.lubumtaximaster.Common;

import android.location.Location;

import com.lubumtaxi.lubumtaximaster.Models.User;
import com.lubumtaxi.lubumtaximaster.Remote.FCMClient;
import com.lubumtaxi.lubumtaximaster.Remote.IFCMService;
import com.lubumtaxi.lubumtaximaster.Remote.IGoogleAPI;
import com.lubumtaxi.lubumtaximaster.Remote.RetrofitClient;

public class Common {

    public static String currentToken = ""; // actual token

    public static Location LastLocation;
    public static final String driver_tbl = "Drivers"; // contain location of drivers
    public static final String user_driver_tbl = "DriversInformation";//contain identificale information of drivers
    public static final String user_rider_tbl = "RidersInformation";
    public static final String pickup_request_tbl = "PickupRequest";
    public static String tokens_tbl="tokens";
    public static User currentUser;

    public static final String baseURL = "http://maps.googleapis.com";
    public static final String fcmURL = "http://fcm.googleapis.com";


    public static IGoogleAPI getGoogleAPI(){
        return RetrofitClient.getClient(baseURL).create(IGoogleAPI.class);
    }
    public static IFCMService getFCMService(){
        return FCMClient.getClient(fcmURL).create(IFCMService.class);
    }

}
