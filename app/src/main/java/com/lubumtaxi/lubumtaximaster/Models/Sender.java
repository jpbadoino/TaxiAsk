package com.lubumtaxi.lubumtaximaster.Models;

public class Sender {
    public String to;
    public Notification notification;

    public Sender() {
    }

    public Sender(String to, Notification notification) {
        this.to = to;
        this.notification = notification;
    }

    public String getTo() {
        return to;
    }

    public Notification getNotification() {
        return notification;
    }
}
